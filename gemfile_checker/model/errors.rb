class GemfileValidatorError < StandardError
end

class EmptyFileError < GemfileValidatorError
  def initialize(msg="Gemfile is empty.")
    super
  end
end

class MissingRubyVersionError < GemfileValidatorError
  def initialize(msg="Gemfile is empty.")
    super
  end
end

class MissingSourceError < GemfileValidatorError
  def initialize(msg="Gemfile is empty.")
    super
  end
end

class SourceAfterRubyVersionError < GemfileValidatorError
  def initialize(msg="Gemfile is empty.")
    super
  end
end

class GemsNotInOrderError < GemfileValidatorError
  def initialize(msg="Gemfile is empty.")
    super
  end
end

class InvalidTokenError < GemfileValidatorError
  def initialize(msg="Gemfile is empty.")
    super
  end
end

class InvalidTokenOrderError < GemfileValidatorError
  def initialize(msg="Gemfile is empty.")
    super
  end
end
