require_relative './errors'

class Validator

  def initialize
    @ruby_line = @source_line = 0
    @invalid_token = false
    @gem_lines, @gem_lines_index = [], []
  end

  def process(gemfile_content)
    gemfile_content.split(/\n/).each.with_index(1) { |line, index|
      if /^ruby '[0-9].[0-9].[0-9]'/.match(line)
        @ruby_line = index
      
      elsif /^source 'https?:\/\/[\S]+'/.match(line)
        @source_line = index
      
      elsif /^gem/.match(line)
        @gem_lines.push(line)
        @gem_lines_index.push(index)

      elsif /^\s*$/.match(line)
        next
      
      else
        @invalid_token = true
      end
    }

    judge(gemfile_content)

    rescue GemfileValidatorError => ex
      puts "#{ex.class}: #{ex.message}"
      return false
    else
      return true
  end

  def judge(gemfile_content)
    raise EmptyFileError unless !gemfile_content.empty?

    raise MissingRubyVersionError unless !@ruby_line.zero?

    raise MissingSourceError unless !@source_line.zero?

    raise SourceAfterRubyVersionError unless @source_line < @ruby_line

    raise GemsNotInOrderError unless @gem_lines == @gem_lines.sort

    raise InvalidTokenError unless !@invalid_token

    raise InvalidTokenOrderError unless @gem_lines_index.min > @ruby_line
  end

end
