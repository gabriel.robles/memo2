
# parser => file_reader
# validator => model
# presenter => print
require_relative './model/validator'
require_relative './model/errors'

gemfile_content = File.read ARGV[0]

validation_result = Validator.new.process gemfile_content

if (validation_result)
  puts 'ok'
else
  puts 'error'
end
    
