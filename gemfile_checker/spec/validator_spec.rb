require 'rspec'
require_relative '../model/validator'

describe Validator do

  subject { @validator = Validator.new }

  it { should respond_to (:process) }

  it 'should return false when Gemfile is empty' do
    gemfile_content = File.read  "#{__dir__}/samples/Gemfile.invalid.empty"
    validation_result = Validator.new.process gemfile_content
    expect(validation_result).to eq false
  end

  it 'should return false when invalid Ruby Version in Gemfile' do
    gemfile_content = File.read  "#{__dir__}/samples/Gemfile.invalid.ruby"
    validation_result = Validator.new.process gemfile_content
    expect(validation_result).to eq false
  end

  it 'should return false when invalid Ruby Source in Gemfile' do
    gemfile_content = File.read  "#{__dir__}/samples/Gemfile.invalid.source"
    validation_result = Validator.new.process gemfile_content
    expect(validation_result).to eq false
  end

  it 'should return false when Source is after Ruby' do
    gemfile_content = File.read  "#{__dir__}/samples/Gemfile.invalid.order"
    validation_result = Validator.new.process gemfile_content
    expect(validation_result).to eq false
  end

  it 'should return false when invalid gem order' do
    gemfile_content = File.read  "#{__dir__}/samples/Gemfile.invalid.gem.order"
    validation_result = Validator.new.process gemfile_content
    expect(validation_result).to eq false
  end

  it 'should return false when invalid tokens order' do
    gemfile_content = File.read  "#{__dir__}/samples/Gemfile.invalid.order2"
    validation_result = Validator.new.process gemfile_content
    expect(validation_result).to eq false
  end

  it 'should return false when invalid token' do
    gemfile_content = File.read  "#{__dir__}/samples/Gemfile.invalid.token"
    validation_result = Validator.new.process gemfile_content
    expect(validation_result).to eq false
  end

  it 'should return true when valid Gemfile' do
    gemfile_content = File.read  "#{__dir__}/samples/Gemfile.valid"
    validation_result = Validator.new.process gemfile_content
    expect(validation_result).to eq true
  end

end
